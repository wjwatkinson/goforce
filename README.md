# goforce

Simple package for interacting with the Salesforce API that currently supports interacting with SObjects. The only piece of magic this package does is logging back in for you if you are making a JSON request and get a 401 (Salesforce sessions expire after 2 hours).

## Login
```
import gitlab.com/wjwatkinson/goforce

session := goforce.Login("username", "password", "token", "clientID", DefautlAPIVersion, DefaultLoginURL, http.Client)
```

## Query
```
q := "select id, account.name from contact where email = 'example@email.com'"
qResp, err := session.SOQuery(q)
if err != nil {
  // handle error
}

if qResp.Success == false {
  // handle unsuccessful request
  errCode := upResp.Errors[0].ErrorCode
  errMsg := upResp.Error[0].Message
}

records := qResp.Records
```

## Update
```
upData := map[string]interface{}{
  "FirstName": "John",
  "LastNAme": "Doe",
}

upResp, err := session.SOUpdate("contact", "contact-id", upData)
if err != nil {
  // handle error
}

if upResp.Success == false {
  // handle unsuccessful request
}
```

## Create
```
data := map[string]interface{}{
  "FirstName": "John",
  "LastName": "Doe",
  "Email": "john.doe@gmail.com",
}

crResp, err := session.SOCreate("contact", data)
if err != nil {
  // handle error
}

if crResp.Success == false {a
  // handle unsuccessful request
}

contID := crResp.ID
```

## Upsert
```
data := map[string]interface{}{
  "FirstName": "John",
  "LastName": "Doe",
  "Email": "john.doe@gmail.com",
  "shopify_id__c": "12335",
}

upsResp, err := session.SOUpsert("contact", "shopify_id__c", "12335", data)
if err != nil {
  // handle error
}

if crResp.Success == false {
  // handle unsuccessful request
}

if crResp.Created == true {
  // handle any additional logic for newly created contacts
}

contID := crResp.ID
```

## Delete
```
resp, err := session.SODelete("contact", "contact-id")
if err != nil {
  // handle error
}

if resp.Success == false {
  // handle unsuccessful request
}
```

