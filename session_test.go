package goforce

import (
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
)

func LoginSuccessMock() {
	loginResp := `<?xml version="1.0" encoding="utf-8" ?>
		<env:Envelope>
			<env:Body>
				<env:loginResponse>
					<env:result>
						<env:serverUrl>https://na0-api.salesforce.com/services/Soap/c/2.5</env:serverUrl>
						<env:sessionId>sessionId</env:sessionId>
						<env:userId>userId</env:userId>
						<env:userInfo>
							<env:userEmail>userEmail</env:userEmail>
							<env:userFullName>userFullName</env:userFullName>
							<env:userName>userName</env:userName>
						</env:userInfo>
					</env:result>
				</env:loginResponse>
			</env:Body>
		</env:Envelope>`
	mockURL := "https://login.salesforce.com/services/Soap/u/" + DefaultAPIVersion
	httpmock.RegisterResponder("POST", mockURL,
		httpmock.NewStringResponder(200, loginResp))
}

func TestLoginPass(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	LoginSuccessMock()

	_, err := Login("username", "password", "token", "clientID", DefaultAPIVersion, DefaultLoginURL, http.Client{})
	if err != nil {
		log.Println(err)
		t.Fail()
	}
}

func TestLoginFail(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	loginErrResp := `<?xml version="1.0" encoding="UTF-8"?>
		<soapenv:Envelope
			xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
			xmlns:sf="urn:fault.partner.soap.sforce.com"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<soapenv:Body>
				<soapenv:Fault>
					<faultcode>INVALID_LOGIN</faultcode>
					<faultstring>INVALID_LOGIN: Invalid username, password, security token; or user locked out.</faultstring>
					<detail>
						<sf:LoginFault xsi:type="sf:LoginFault">
						<sf:exceptionCode>INVALID_LOGIN</sf:exceptionCode>
						<sf:exceptionMessage>Invalid username, password, security token; or user locked out.</sf:exceptionMessage>
						</sf:LoginFault>
					</detail>
				</soapenv:Fault>
			</soapenv:Body>
		</soapenv:Envelope>`
	mockURL := "https://login.salesforce.com/services/Soap/u/" + DefaultAPIVersion
	httpmock.RegisterResponder("POST", mockURL,
		httpmock.NewStringResponder(500, loginErrResp))

	_, err := Login("username", "password", "token", "clientID", DefaultAPIVersion, DefaultLoginURL, http.Client{})
	if err.Error() != "500 | INVALID_LOGIN - INVALID_LOGIN: Invalid username, password, security token; or user locked out." {
		log.Println("err: " + err.Error())
		t.Fail()
	}
}

func MakeTestSession() *Session {
	return &Session{
		InstanceURL: "https://na0-api.salesforce.com",
		ID:          "SessionID",
		Username:    "username",
		Password:    "password",
		Token:       "token",
		ClientID:    "clientID",
		APIVersion:  DefaultAPIVersion,
		LoginURL:    "https://login.salesforce.com",
		HTTPClient:  http.Client{},
	}
}

func TestJSONRequestSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()

	u := "https://na0-api.salesforce.com/services/sobjects/contact"

	mockResp := `{
  	"id" : "001D000000IqhSLIAZ",
  	"errors" : [ ],
  	"success" : true
	}`

	httpmock.RegisterResponder("POST", u,
		httpmock.NewStringResponder(200, mockResp))

	data := map[string]interface{}{
		"FirstName": "John",
		"LastNAme":  "Doe",
	}
	jsResp, err := session.JSONRequest(http.MethodPost, u, data)
	if err != nil {
		log.Println(err)
		t.Fail()
	}
	if jsResp.Success != true || jsResp.StatusCode != 200 {
		fmt.Printf("%+v\n", jsResp)
		t.Fail()
	}
}

func TestJSONRequestFail(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()
	u := "https://na0-api.salesforce.com/services/sobjects/contact"

	mockResp := `{
  	"id" : "001D000000IqhSLIAZ",
  	"errors" : [ ],
  	"success" : true
	}`

	httpmock.RegisterResponder("POST", u,
		httpmock.NewStringResponder(400, mockResp))

	data := map[string]interface{}{
		"FirstName": "John",
		"LastNAme":  "Doe",
	}
	jsResp, err := session.JSONRequest(http.MethodPost, u, data)
	if err != nil {
		log.Println(err)
		t.Fail()
	}
	if jsResp.Success != false || jsResp.StatusCode != 400 {
		fmt.Printf("%+v\n", jsResp)
		t.Fail()
	}
}

func TestJSONRequestExpiredSessionSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()
	u := "https://na0-api.salesforce.com/services/sobjects/contact"

	mockResp := `{
  	"id" : "001D000000IqhSLIAZ",
  	"errors" : [ ],
  	"success" : true
	}`

	httpmock.RegisterResponder("POST", u,
		httpmock.NewStringResponder(401, mockResp))

	LoginSuccessMock()

	data := map[string]interface{}{
		"FirstName": "John",
		"LastNAme":  "Doe",
	}
	jsResp, err := session.JSONRequest(http.MethodPost, u, data)
	if err != nil {
		log.Println(err)
		t.Fail()
	}
	if jsResp.Success != false || jsResp.StatusCode != 401 {
		fmt.Printf("%+v\n", jsResp)
		t.Fail()
	}

	callInfo := httpmock.GetCallCountInfo()
	insCallCount := callInfo[fmt.Sprintf("POST %s", u)]
	loginCallCount := callInfo["POST https://login.salesforce.com/services/Soap/u/"+DefaultAPIVersion]

	if insCallCount != 2 || loginCallCount != 1 {
		fmt.Printf("Insert Call Count: %v, Login Call Count: %v", insCallCount, loginCallCount)
		t.Fail()
	}
}
