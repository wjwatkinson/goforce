package goforce

import (
	"fmt"
	"log"
	"net/url"
	"testing"

	"github.com/jarcoal/httpmock"
)

func TestDescribeSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/sobjects/contact/describe"

	httpmock.RegisterResponder("GET", mockURL,
		httpmock.NewStringResponder(200, `{}`))

	resp, err := session.SODescribe("contact")
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if resp.StatusCode != 200 || resp.Success != true {
		fmt.Printf("%+v\n", resp)
		t.Fail()
	}
}

func TestQuerySuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()

	q := "select id from contact where email = 'test@gmail.com'"

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/query/?q=" + url.PathEscape(q)

	mockResp := `{
    "done" : true,
    "totalSize" : 14,
    "records" : []
	}`

	httpmock.RegisterResponder("GET", mockURL,
		httpmock.NewStringResponder(200, mockResp))

	resp, err := session.SOQuery(q)
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if resp.TotalSize != 14 || resp.Done != true {
		fmt.Printf("%+v\n", resp)
		t.Fail()
	}
}

func TestCreateSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/sobjects/contact"

	mockResp := `{
  	"id" : "001D000000IqhSLIAZ",
  	"errors" : [ ],
  	"success" : true
	}`

	httpmock.RegisterResponder("POST", mockURL,
		httpmock.NewStringResponder(200, mockResp))

	data := map[string]interface{}{
		"FirstName": "John",
		"LastNAme":  "Doe",
	}

	cont, err := session.SOCreate("contact", data)
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if cont.StatusCode != 200 || cont.Success != true || cont.ID != "001D000000IqhSLIAZ" {
		fmt.Printf("%+v\n", cont)
		t.Fail()
	}

	callInfo := httpmock.GetCallCountInfo()
	insCallCount := callInfo[fmt.Sprintf("POST %s", mockURL)]
	if insCallCount != 1 {
		fmt.Printf("Insert Call Count %v instead of 1", insCallCount)
		t.Fail()
	}
}

func TestUpsertSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	session := MakeTestSession()

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/sobjects/contact/ext_id_fname__c/ext_id"
	mockResp := `{
    "id" : "00190000001pPvHAAU",
    "errors" : [ ],
    "success" : true,
    "created": true
	}`
	httpmock.RegisterResponder("POST", mockURL,
		httpmock.NewStringResponder(200, mockResp))

	reqData := map[string]interface{}{
		"FirstName": "John",
		"LastName":  "Doe",
	}
	resp, err := session.SOUpsert("contact", "ext_id_fname__c", "ext_id", reqData)
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if resp.Success == false || resp.Created == false {
		fmt.Printf("%+v\n", resp)
		t.Fail()
	}

	// ensure ext id was set
	if reqData["ext_id_fname__c"] != "ext_id" {
		log.Printf("%+v/n", reqData)
		t.Fail()
	}

	callInfo := httpmock.GetCallCountInfo()
	insCallCount := callInfo[fmt.Sprintf("POST %s", mockURL)]
	if insCallCount != 1 {
		fmt.Printf("Expected 1 call, but got %v", insCallCount)
	}
}

func TestUpdateSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/sobjects/contact/contact_id"
	httpmock.RegisterResponder("PATCH", mockURL,
		httpmock.NewStringResponder(200, ""))

	reqData := map[string]interface{}{
		"FirstName": "John",
	}
	session := MakeTestSession()
	resp, err := session.SOUpdate("contact", "contact_id", reqData)
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if resp.Success == false || resp.StatusCode != 200 {
		fmt.Printf("%+v\n", resp)
		t.Fail()
	}

	callInfo := httpmock.GetCallCountInfo()
	insCallCount := callInfo[fmt.Sprintf("PATCH %s", mockURL)]
	if insCallCount != 1 {
		fmt.Printf("Expected 1 call, but got %v", insCallCount)
	}
}

func TestDeleteSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockURL := "https://na0-api.salesforce.com/services/data/v50.0/sobjects/contact/contact_id"
	httpmock.RegisterResponder("DELETE", mockURL,
		httpmock.NewStringResponder(200, ""))

	session := MakeTestSession()

	resp, err := session.SODelete("contact", "contact_id")
	if err != nil {
		log.Println(err)
		t.Fail()
	}

	if resp.Success == false || resp.StatusCode != 200 {
		fmt.Printf("%+v\n", resp)
		t.Fail()
	}

	callInfo := httpmock.GetCallCountInfo()
	insCallCount := callInfo[fmt.Sprintf("DELETE %s", mockURL)]
	if insCallCount != 1 {
		fmt.Printf("Expected 1 call, but got %v", insCallCount)
		t.Fail()
	}

}
