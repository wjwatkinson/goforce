package goforce

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"html"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const (
	DefaultAPIVersion = "50.0"
	DefaultClientID   = "goforce"
	DefaultLoginURL   = "https://login.salesforce.com"
)

type Session struct {
	InstanceURL string
	ID          string
	Username    string
	Password    string
	Token       string
	ClientID    string
	APIVersion  string
	LoginURL    string
	HTTPClient  http.Client
}

func ParseXMLError(resp *http.Response) error {
	var reqError struct {
		Message   string `xml:"Body>Fault>faultstring"`
		ErrorCode string `xml:"Body>Fault>faultcode"`
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	err := xml.Unmarshal(buf.Bytes(), &reqError)
	if err != nil {
		return err
	}
	errorString := fmt.Sprintf("%v | %v - %v", resp.StatusCode, reqError.ErrorCode, reqError.Message)
	return errors.New(errorString)
}

// LoginPassword signs into salesforce using password. token is optional if trusted IP is configured.
// Ref: https://developer.salesforce.com/docs/atlas.en-us.214.0.api_rest.meta/api_rest/intro_understanding_username_password_oauth_flow.htm
// Ref: https://developer.salesforce.com/docs/atlas.en-us.214.0.api.meta/api/sforce_api_calls_login.htm
func Login(username string, password string, token string, clientID string, apiVersion string, loginURL string, httpClient http.Client) (*Session, error) {
	// Use the SOAP interface to acquire session ID with username, password, and token.
	// Do not use REST interface here as REST interface seems to have strong checking against client_id, while the SOAP
	// interface allows a non-exist placeholder client_id to be used.
	soapBody := `<?xml version="1.0" encoding="utf-8" ?>
        <env:Envelope
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:urn="urn:partner.soap.sforce.com">
            <env:Header>
                <urn:CallOptions>
                    <urn:client>%s</urn:client>
                    <urn:defaultNamespace>sf</urn:defaultNamespace>
                </urn:CallOptions>
            </env:Header>
            <env:Body>
                <n1:login xmlns:n1="urn:partner.soap.sforce.com">
                    <n1:username>%s</n1:username>
                    <n1:password>%s%s</n1:password>
                </n1:login>
            </env:Body>
        </env:Envelope>`
	soapBody = fmt.Sprintf(soapBody, clientID, username, html.EscapeString(password), token)

	u := fmt.Sprintf("%s/services/Soap/u/%s", loginURL, apiVersion)
	req, err := http.NewRequest(http.MethodPost, u, strings.NewReader(soapBody))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("charset", "UTF-8")
	req.Header.Add("SOAPAction", "login")

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, ParseXMLError(resp)
	}

	respData, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	var loginResponse struct {
		XMLName      xml.Name `xml:"Envelope"`
		ServerURL    string   `xml:"Body>loginResponse>result>serverUrl"`
		SessionID    string   `xml:"Body>loginResponse>result>sessionId"`
		UserID       string   `xml:"Body>loginResponse>result>userId"`
		UserEmail    string   `xml:"Body>loginResponse>result>userInfo>userEmail"`
		UserFullName string   `xml:"Body>loginResponse>result>userInfo>userFullName"`
		UserName     string   `xml:"Body>loginResponse>result>userInfo>userName"`
	}

	err = xml.Unmarshal(respData, &loginResponse)
	if err != nil {
		return nil, err
	}

	parsedServerURL, err := url.Parse(loginResponse.ServerURL)
	if err != nil {
		return nil, err
	}
	serverDomain := fmt.Sprintf("https://%s", parsedServerURL.Host)

	session := Session{
		InstanceURL: serverDomain,
		ID:          loginResponse.SessionID,
		Username:    username,
		Password:    password,
		Token:       token,
		ClientID:    clientID,
		APIVersion:  apiVersion,
		LoginURL:    loginURL,
		HTTPClient:  httpClient,
	}
	return &session, nil
}

type JSONErrors []struct {
	Message   string `json:"message"`
	ErrorCode string `json:"erorrCode"`
}

type JSONResponse struct {
	Body       []byte
	Success    bool
	StatusCode int
	Errors     JSONErrors
}

func (session *Session) JSONRequest(method, u string, data map[string]interface{}) (*JSONResponse, error) {
	reqData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(method, u, bytes.NewReader(reqData))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", session.ID))
	req.Header.Add("Content-Type", "application/json")

	resp, err := session.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	// If Unauthorized, or Session Expired Response
	//Try Login to get a new token and try the request again
	if resp.StatusCode == 401 {
		session, err = Login(session.Username, session.Password, session.Token, session.ClientID, session.APIVersion, session.LoginURL, session.HTTPClient)
		if err != nil {
			return nil, err
		}
		resp, err = session.HTTPClient.Do(req)
		if err != nil {
			return nil, err
		}
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var errResp JSONErrors
	success := true

	if resp.StatusCode < 200 || resp.StatusCode > 399 {
		err = json.Unmarshal(body, &errResp)
		success = false
	}

	jsResp := JSONResponse{
		Body:       body,
		Success:    success,
		StatusCode: resp.StatusCode,
		Errors:     errResp,
	}

	return &jsResp, nil
}
