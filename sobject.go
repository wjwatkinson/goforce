package goforce

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func (session *Session) MakeSObjectsURL() string {
	session.APIVersion = strings.Replace(session.APIVersion, "v", "", -1)
	retURL := fmt.Sprintf("%s/services/data/v%s/sobjects/", session.InstanceURL, session.APIVersion)
	return retURL
}

type DescribeResponse struct {
	Body       map[string]interface{}
	Success    bool
	StatusCode int
	Errors     JSONErrors
}

func (session *Session) SODescribe(sobject string) (*DescribeResponse, error) {
	u := session.MakeSObjectsURL() + sobject + "/describe"

	jsResp, err := session.JSONRequest(http.MethodGet, u, nil)
	if err != nil {
		return nil, err
	}

	var descBody map[string]interface{}
	err = json.Unmarshal(jsResp.Body, &descBody)
	if err != nil {
		return nil, err
	}

	descResp := DescribeResponse{
		Body:       descBody,
		Success:    jsResp.Success,
		StatusCode: jsResp.StatusCode,
		Errors:     jsResp.Errors,
	}

	return &descResp, nil
}

// QueryResult holds the response data from an SOQL query.
type QueryResult struct {
	TotalSize      int                      `json:"totalSize"`
	Done           bool                     `json:"done"`
	NextRecordsURL string                   `json:"nextRecordsUrl"`
	Records        []map[string]interface{} `json:"records"`
	Success        bool
	StatusCode     int
}

// Perform SOQL Query
func (session *Session) SOQuery(query string) (*QueryResult, error) {
	session.APIVersion = strings.Replace(session.APIVersion, "v", "", -1)
	baseURL := fmt.Sprintf("%s/services/data/v%s/", session.InstanceURL, session.APIVersion)
	u := fmt.Sprintf("%squery/?q=%s", baseURL, url.PathEscape(query))
	jsResp, err := session.JSONRequest(http.MethodGet, u, nil)
	if err != nil {
		return nil, err
	}

	var result struct {
		TotalSize      int                      `json:"totalSize"`
		Done           bool                     `json:"done"`
		NextRecordsURL string                   `json:"nextRecordsUrl"`
		Records        []map[string]interface{} `json:"records"`
	}

	err = json.Unmarshal(jsResp.Body, &result)
	if err != nil {
		return nil, err
	}

	qRes := QueryResult{
		TotalSize:      result.TotalSize,
		Done:           result.Done,
		NextRecordsURL: result.NextRecordsURL,
		Records:        result.Records,
		Success:        jsResp.Success,
		StatusCode:     jsResp.StatusCode,
	}

	return &qRes, nil
}

type CreateResponse struct {
	ID         string
	StatusCode int
	Success    bool
	Errors     JSONErrors
}

// Create
// https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_sobject_create.htm
func (session *Session) SOCreate(sobject string, data map[string]interface{}) (*CreateResponse, error) {
	u := session.MakeSObjectsURL() + sobject

	resp, err := session.JSONRequest(http.MethodPost, u, data)
	if err != nil {
		return nil, err
	}

	var insResp struct {
		ID      string `json:"id"`
		Success bool   `json:"success"`
	}

	err = json.Unmarshal(resp.Body, &insResp)
	if err != nil {
		return nil, err
	}

	crResp := CreateResponse{
		ID:         insResp.ID,
		Success:    insResp.Success,
		StatusCode: resp.StatusCode,
		Errors:     resp.Errors,
	}

	return &crResp, nil
}

type UpsertResponse struct {
	ID         string
	Created    bool
	StatusCode int
	Success    bool
	Errors     JSONErrors
}

// Upsert
// https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_upsert.htm
func (session *Session) SOUpsert(sobject string, extIDFName string, extID string, data map[string]interface{}) (*UpsertResponse, error) {
	// ensure extID is set in the payload
	if data[extIDFName] == nil {
		data[extIDFName] = extID
	}

	u := fmt.Sprintf("%s%s/%s/%s", session.MakeSObjectsURL(), sobject, extIDFName, extID)

	jsResp, err := session.JSONRequest(http.MethodPost, u, data)
	if err != nil {
		return nil, err
	}

	var upsBody struct {
		ID      string `json:"id"`
		Created bool   `json:"created"`
	}

	if jsResp.Success {
		err = json.Unmarshal(jsResp.Body, &upsBody)
		if err != nil {
			return nil, err
		}
	}

	upsResp := UpsertResponse{
		ID:         upsBody.ID,
		Created:    upsBody.Created,
		StatusCode: jsResp.StatusCode,
		Success:    jsResp.Success,
		Errors:     jsResp.Errors,
	}
	return &upsResp, nil
}

// Update
// https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_update_fields.htm
func (session *Session) SOUpdate(sobject string, id string, data map[string]interface{}) (*JSONResponse, error) {
	u := fmt.Sprintf("%v%v/%v", session.MakeSObjectsURL(), sobject, id)
	jsResp, err := session.JSONRequest(http.MethodPatch, u, data)
	if err != nil {
		return nil, err
	}
	return jsResp, nil
}

// Delete
// https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_delete_record.htm
func (session *Session) SODelete(sobject string, id string) (*JSONResponse, error) {
	u := fmt.Sprintf("%v%v/%v", session.MakeSObjectsURL(), sobject, id)
	jsResp, err := session.JSONRequest(http.MethodDelete, u, nil)
	if err != nil {
		return nil, err
	}
	return jsResp, nil
}
